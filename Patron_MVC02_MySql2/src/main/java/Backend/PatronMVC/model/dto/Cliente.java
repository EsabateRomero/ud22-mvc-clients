package Backend.PatronMVC.model.dto;

public class Cliente {
	
	private Integer idCliente;
	private String nombreCliente;
	private String apellidoCliente;
	private String direccionCliente;
	private Integer DNICliente;
	
	/**
	 * @return the idPersona
	 */
	public Integer getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idPersona the idPersona to set
	 */
	/*public void setIdPersona(Integer idPersona) {
		this.idCliente = idPersona;
	}*/
	/**
	 * @return the nombrePersona
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	
	/**
	 * @param nombreCliente the nombrePersona to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * @return the edadPersona
	 */
	public String getApellidoCliente() {
		return apellidoCliente;
	}
	/**
	 * @param edadPersona the edadPersona to set
	 */
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}
	/**
	 * @return the profesionPersona
	 */
	public String getDireccionCliente() {
		return direccionCliente;
	}
	/**
	 * @param direccionCliente the profesionPersona to set
	 */
	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
	/**
	 * @return the telefonoPersona
	 */
	public Integer getDNICliente() {
		return DNICliente;
	}
	/**
	 * @param telefonoPersona the telefonoPersona to set
	 */
	public void setDNICliente(Integer dniCliente) {
		this.DNICliente = dniCliente;
	}
	@Override
	public String toString() {
		return "Cliente [idCliente=" + idCliente + ", nombreCliente=" + nombreCliente + ", apellidoCliente="
				+ apellidoCliente + ", direccionCliente=" + direccionCliente + ", DNICliente=" + DNICliente + "]";
	}
	

}
