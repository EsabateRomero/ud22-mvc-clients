package Backend.PatronMVC.model.conexion;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Clase que permite conectar con la base de datos
 *
 */
public class Conexion {
   static String bd = "Activitat1Clients";
   static String login = "remote";
   static String password = "12345Remote<3!";
   static String url = "jdbc:mysql://192.168.1.135:3306?useTimezone=true&serverTimezone=UTC";

   Connection conexion = null;

   // Constructor de DbConnection 
   public Conexion() {
      try{
         //obtenemos el driver de para mysql
         Class.forName("com.mysql.cj.jdbc.Driver");
         //obtenemos la conexion
         conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.135:3306?"
					+ "useTimezone=true&serverTimezone=UTC","remote","12345Remote<3!");
      
		 createDB(bd, conexion); 
		 try {
			 
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);

		 } catch (Exception e) {
			 
			System.out.println("No s'ha pogut connectar amb la bd");
			
		}
		 
         if (conexion!=null){
            System.out.print("Conexión a base de datos "+bd+"_SUCCESS at");
            fecha();
         }
      }
      catch(SQLException e){
         System.out.println(e);
      }catch(ClassNotFoundException e){
         System.out.println(e);
      }catch(Exception e){
         System.out.println(e);
      }
   }
   
   //destruimos la base de dadas
	public static void eliminarDB(String name, Connection conexion) {
		try {
			String Query = "DROP DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (SQLException ex) {
		
		}
	}

	
	//Creamos la bd antes de usarla
	public static void createDB(String name, Connection conexion) {
		try {
			String Query = "CREATE DATABASE IF NOT EXISTS " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (SQLException ex) {
			System.out.println("No se ha podido crear la base de datos");
		}
	}

   //Permite retornar la conexion
   public Connection getConnection(){
      return conexion;
   }

   public void desconectar(){
      conexion = null;
   }
   
 //METODO QUE MUESTRA FECHA
 	public static void fecha() {
 		Date date = new Date();
 		DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
 		System.out.println(" - " + hourdateFormat.format(date));
 		}

	public static String getBd() {
		return bd;
	}

	public static void setBd(String bd) {
		Conexion.bd = bd;
	}
 	
 	
 	/* GETS I SETTERS */
 	
 	


}