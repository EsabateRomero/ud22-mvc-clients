package Backend.PatronMVC.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import Backend.PatronMVC.controller.ClienteController;

public class VentanaPrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private ClienteController personaController; //objeto PersonaController que permite la relacion entre esta clase y la clase PersonaController
	private JButton registrarse, iniciarSessio;
	

	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana principal
	 */
	public VentanaPrincipal() {
		getContentPane().setBackground(new Color(152, 251, 152));
		setBounds(100, 100, 450, 196);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("CLIENTES");
		setLocationRelativeTo(null);
		setResizable(false);
		
		JLabel titol = new JLabel("BENVOLGUT CLIENT!");
		titol.setFont(new Font("Century Gothic", Font.PLAIN, 43));
		
		registrarse = new JButton("Registra't!");
		registrarse.setFont(new Font("Century Gothic", Font.PLAIN, 22));
		registrarse.setBackground(new Color(242,100,25));
		
		iniciarSessio = new JButton("Inicia sessi\u00F3");
		iniciarSessio.setFont(new Font("Century Gothic", Font.PLAIN, 22));
		iniciarSessio.setBackground(new Color(68, 161, 160));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(titol)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(registrarse, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(iniciarSessio, GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(titol)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(registrarse, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
						.addComponent(iniciarSessio, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);

		
		registrarse.addActionListener(this);
		iniciarSessio.addActionListener(this);
	}


	public void setCoordinador(ClienteController personaController) {
		this.personaController=personaController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==registrarse) {
			personaController.mostrarVentanaRegistro();	
			
			
	
		}
		if (e.getSource()==iniciarSessio) {
			personaController.mostrarVentanaConsulta();			
		}
	}
}
