package Backend.PatronMVC.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import Backend.PatronMVC.controller.ClienteController;
import Backend.PatronMVC.model.dto.Cliente;



public class VentanaRegistro extends JFrame implements ActionListener{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ClienteController clienteController; //objeto personaController que permite la relacion entre esta clase y la clase PersonaController
	private JTextField textNombre;
	private JTextField textApellido;
	private JTextField textDireccion;
	private JTextField textDNI;
	private JButton Registrar, Cancelar;
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana de registro
	 */
	public VentanaRegistro() {

	
		getContentPane().setBackground(new Color(152, 251, 152));
		setBounds(100, 100, 380, 385);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("CLIENTES REGISTRE");
		setLocationRelativeTo(null);
		setResizable(false);
		
		JLabel titol = new JLabel("REGISTRA'T");
		titol.setFont(new Font("Century Gothic", Font.PLAIN, 43));
		
		JLabel Nombre = new JLabel("Nombre:");
		Nombre.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		JLabel Apellido = new JLabel("Apellido:");
		Apellido.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		JLabel Direccion = new JLabel("Direccion: ");
		Direccion.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		JLabel dni = new JLabel("DNI:");
		dni.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		textNombre = new JTextField();
		textNombre.setBackground(new Color(32, 178, 170));
		textNombre.setColumns(10);
		
		textApellido = new JTextField();
		textApellido.setBackground(new Color(211, 211, 211));
		textApellido.setColumns(10);
		
		textDireccion = new JTextField();
		textDireccion.setBackground(new Color(210, 105, 30));
		textDireccion.setColumns(10);
		
		textDNI = new JTextField();
		textDNI.setBackground(new Color(233, 150, 122));
		textDNI.setColumns(10);
		
		Registrar = new JButton("Registrar");
		Registrar.setBackground(new Color(238, 130, 238));
		Registrar.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		Cancelar = new JButton("Cancelar");
		Cancelar.setBackground(new Color(255, 165, 0));
		Cancelar.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(titol))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(27)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(Apellido, GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
								.addComponent(Nombre, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(dni, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(Direccion, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
								.addComponent(textDireccion, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
								.addComponent(textApellido, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
								.addComponent(textNombre, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(45)
							.addComponent(Registrar, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(Cancelar, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(titol)
					.addGap(30)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(textNombre)
						.addComponent(Nombre, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(Apellido, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(textApellido, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(Direccion, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(textDireccion, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(dni, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addGap(59)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(Registrar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(Cancelar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
					.addGap(33))
		);
		getContentPane().setLayout(groupLayout);
		
		Registrar.addActionListener(this);
		Cancelar.addActionListener(this);
	}

	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==Registrar){
			try {
				
				Cliente miCliente=new Cliente();
				miCliente.setNombreCliente(textNombre.getText());
				miCliente.setApellidoCliente(textApellido.getText());
				miCliente.setDireccionCliente(textDireccion.getText());
				miCliente.setDNICliente(Integer.parseInt(textDNI.getText()));
				
				clienteController.registrarCliente(miCliente);	
				
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource()==Cancelar)
		{
			this.dispose();
		}
	}
	
}
	

