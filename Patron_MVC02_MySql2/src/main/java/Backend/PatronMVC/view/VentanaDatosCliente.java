package Backend.PatronMVC.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import Backend.PatronMVC.model.dto.Cliente;
import Backend.PatronMVC.model.service.ClienteServ;
import Backend.PatronMVC.controller.ClienteController;

public class VentanaDatosCliente  extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private ClienteController clienteController; //objeto personaController que permite la relacion entre esta clase y la clase personaController
	private JTextField textNombre, textApellidos, textDireccion, textDNI;
	private JLabel titol, Nombre, Apellidos, Direccion;
	private JButton Modificar, Cancelar, Eliminar;
	private Cliente miCliente;
	
	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;

	}


	public VentanaDatosCliente(Cliente miCliente) {

		
		getContentPane().setBackground(new Color(152, 251, 152));
		setBounds(100, 100, 348, 338);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("BUSQUEDA");
		setLocationRelativeTo(null);
		setResizable(false);
		
		titol = new JLabel("B\u00DASQUEDA");
		titol.setFont(new Font("Century Gothic", Font.BOLD, 23));
		
		Nombre = new JLabel("Nombre:");
		Nombre.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		//textNombre.setText(miCliente.getNombreCliente()+"");
		textNombre = new JTextField();
		
		
		textNombre.setEditable(false);
		textNombre.setBackground(new Color(233, 150, 122));
		textNombre.setColumns(10);
	
		
		Modificar = new JButton("Modificar");
		Modificar.setBackground(new Color(238, 130, 238));
		Modificar.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		Cancelar = new JButton("Cancelar");
		Cancelar.setBackground(new Color(255, 165, 0));
		Cancelar.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		Apellidos = new JLabel("Apellidos:");
		Apellidos.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		textApellidos = new JTextField();
		
		textApellidos.setEditable(false);
		textApellidos.setColumns(10);
		textApellidos.setBackground(new Color(0, 204, 204));
		
		Direccion = new JLabel("Direccion:");
		Direccion.setFont(new Font("Century Gothic", Font.PLAIN, 20));

		
		JLabel dni_1_2 = new JLabel("DNI:");
		dni_1_2.setFont(new Font("Century Gothic", Font.PLAIN, 20));

		textDireccion = new JTextField();
		textDireccion.setEditable(false);
		textDireccion.setColumns(10);
		textDireccion.setBackground(new Color(255, 204, 51));
		
		textDNI = new JTextField();
		textDNI.setEditable(false);
		textDNI.setColumns(10);
		textDNI.setBackground(new Color(255, 51, 204));
		
		Eliminar = new JButton("Eliminar");
		Eliminar.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		Eliminar.setBackground(new Color(102, 153, 255));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
												.addComponent(Apellidos, GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
												.addComponent(Nombre, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(Direccion, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(dni_1_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
											.addGap(18)
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
												.addComponent(textDireccion, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
												.addComponent(textApellidos, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
												.addComponent(textNombre, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)))
										.addGroup(groupLayout.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(Modificar, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(Eliminar, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))))
								.addComponent(titol)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(93)
							.addComponent(Cancelar, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(21, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(titol)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(Nombre, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(textNombre, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(textApellidos, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(Apellidos, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(Direccion, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(dni_1_2, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(textDireccion, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(Modificar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(Eliminar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(Cancelar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addGap(105))
		);
		getContentPane().setLayout(groupLayout);
		
		Modificar.addActionListener(this);
		Eliminar.addActionListener(this);
		Cancelar.addActionListener(this);
		
		
	}

	public void iniciar() {
		try {
			textNombre.setText(miCliente.getNombreCliente());
			textApellidos.setText(miCliente.getApellidoCliente());
			textDireccion.setText(miCliente.getDireccionCliente());
			textDNI.setText(miCliente.getDNICliente()+"");
			
		} catch (Exception e2) {
			
		}
	}


	public Cliente getMiCliente() {
		return miCliente;
	}


	public void setMiCliente(Cliente miCliente) {
		this.miCliente = miCliente;
	}


	@Override
	public void actionPerformed(ActionEvent e) 
	{
		
		if (e.getSource()==Modificar)
		{
			if(Modificar.getText().equals("Modificar")) {
				Modificar.setText("Guardar");
				habilita(true, true, true, false);	
			}
			else {
				Modificar.setText("Modificar");
				habilita(false, false, false, false);
				miCliente.setNombreCliente(textNombre.getText());
				miCliente.setApellidoCliente(textApellidos.getText());
				miCliente.setDireccionCliente(textDireccion.getText());
				clienteController.modificarCliente(miCliente);
				
			}
				

		}
		
		if (e.getSource()==Eliminar)
		{
			clienteController.eliminarCliente(miCliente.getDNICliente()+"");
			dispose();
		}
		
		if(e.getSource()==Cancelar) {
			this.dispose();
		}
		
	}

	/**
	 * permite cargar los datos de la persona consultada
	 * @param miPersona
	 */
	/*private void muestraPersona(Persona miPersona) {
		textNombre.setText(miPersona.getNombrePersona());
		textEdad.setText(miPersona.getEdadPersona()+"");
		textTelefono.setText(miPersona.getTelefonoPersona()+"");
		textProfesion.setText(miPersona.getProfesionPersona());
		habilita(true, false, false, false, false, true, false, true, true);
	}*/


	/**
	 * Permite limpiar los componentes
	 */
	
	/*
	public void limpiar()
	{
		textCod.setText("");
		textNombre.setText("");
		textEdad.setText("");
		textTelefono.setText("");
		textProfesion.setText("");
		habilita(true, false, false, false, false, true, false, true, true);
	}
	*/

	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * @param codigo
	 * @param nombre
	 * @param edad
	 * @param tel
	 * @param profesion
	 * @param cargo
	 * @param bBuscar
	 * @param bGuardar
	 * @param bModificar
	 * @param bEliminar
	 */
	

	public void habilita(boolean nombre, boolean apellido, boolean direccion, boolean DNI)
	{

		textNombre.setEditable(nombre);
		textApellidos.setEditable(apellido);
		textDireccion.setEditable(direccion);
		textDNI.setEditable(DNI);
	}
	

}

