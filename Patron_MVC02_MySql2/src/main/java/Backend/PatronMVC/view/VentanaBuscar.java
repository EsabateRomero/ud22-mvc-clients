package Backend.PatronMVC.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import Backend.PatronMVC.model.dto.Cliente;
import Backend.PatronMVC.model.service.ClienteServ;
import Backend.PatronMVC.controller.ClienteController;

public class VentanaBuscar extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private ClienteController clienteController; //objeto personaController que permite la relacion entre esta clase y la clase personaController
	private JTextField textDNI;
	private JLabel titol, dni;
	private JButton Buscar, Cancelar;
	private Cliente miCliente = new Cliente();
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana de busqueda
	 */
	public VentanaBuscar() {

		getContentPane().setBackground(new Color(152, 251, 152));
		setBounds(100, 100, 338, 267);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("CLIENTES BUSCAR");
		setLocationRelativeTo(null);
		setResizable(false);
		
		titol = new JLabel("BUSCAR-SE");
		titol.setFont(new Font("Century Gothic", Font.PLAIN, 43));
		
		dni = new JLabel("DNI:");
		dni.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		textDNI = new JTextField();
		textDNI.setBackground(new Color(233, 150, 122));
		textDNI.setColumns(10);
		
		Buscar = new JButton("Buscar");
		Buscar.setBackground(new Color(238, 130, 238));
		Buscar.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		
		Cancelar = new JButton("Cancelar");
		Cancelar.setBackground(new Color(255, 165, 0));
		Cancelar.setFont(new Font("Century Gothic", Font.PLAIN, 20));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addComponent(dni)
									.addGap(33)
									.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE))
								.addComponent(titol)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addComponent(Buscar, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(Cancelar, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(43, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(titol)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(dni, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addGap(52)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(Buscar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(Cancelar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
					.addGap(142))
		);
		getContentPane().setLayout(groupLayout);
		

		Buscar.addActionListener(this);
		Cancelar.addActionListener(this);
		
	}

	public void setCoordinador(ClienteController personaController) {
		this.clienteController=personaController;
	}
	
	public Cliente getMiCliente() {
		return miCliente;
	}

	public void setMiCliente(Cliente miCliente) {
		this.miCliente = miCliente;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource()==Buscar)
		{
			miCliente = clienteController.buscarCliente(textDNI.getText());
			System.out.println(miCliente);
			if (miCliente!=null) {
				
				clienteController.mostrarVentanaDatos(miCliente);	
			} else if(ClienteServ.consultaCliente==true) {
				JOptionPane.showMessageDialog(null, "La persona no existe","Advertencia", JOptionPane.WARNING_MESSAGE);
			}
				
		}
		if (e.getSource()==Cancelar)
		{
			this.dispose();
		}
	}

	
	
	
	

	/**
	 * permite cargar los datos de la persona consultada
	 * @param miPersona
	 */
	/*private void muestraPersona(Persona miPersona) {
		textNombre.setText(miPersona.getNombrePersona());
		textEdad.setText(miPersona.getEdadPersona()+"");
		textTelefono.setText(miPersona.getTelefonoPersona()+"");
		textProfesion.setText(miPersona.getProfesionPersona());
		habilita(true, false, false, false, false, true, false, true, true);
	}*/


	/**
	 * Permite limpiar los componentes
	 */
	
	/*
	public void limpiar()
	{
		textCod.setText("");
		textNombre.setText("");
		textEdad.setText("");
		textTelefono.setText("");
		textProfesion.setText("");
		habilita(true, false, false, false, false, true, false, true, true);
	}
	*/

	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * @param codigo
	 * @param nombre
	 * @param edad
	 * @param tel
	 * @param profesion
	 * @param cargo
	 * @param bBuscar
	 * @param bGuardar
	 * @param bModificar
	 * @param bEliminar
	 */
	
	/*
	public void habilita(boolean codigo, boolean nombre, boolean edad, boolean tel, boolean profesion,	 boolean bBuscar, boolean bGuardar, boolean bModificar, boolean bEliminar)
	{
		textCod.setEditable(codigo);
		textNombre.setEditable(nombre);
		textEdad.setEditable(edad);
		textTelefono.setEditable(tel);
		textProfesion.setEditable(profesion);
		botonBuscar.setEnabled(bBuscar);
		botonGuardar.setEnabled(bGuardar);
		botonModificar.setEnabled(bModificar);
		botonEliminar.setEnabled(bEliminar);
	}
	
	*/
}

